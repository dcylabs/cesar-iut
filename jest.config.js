module.exports = {
    "collectCoverageFrom": ["src/**/*.js", "!**/node_modules/**", "!**/*.uncov.js"],
    "coverageReporters": ["html", "text", "text-summary", "cobertura"],
    "testMatch": ["**/*.test.js"]
  }
  