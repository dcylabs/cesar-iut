/* istanbul ignore file */

const Cesar = require('./cesar.js'); 
var flags = require('flags');

function main() {

    flags.defineBoolean('e', false, 'Encipher');
    flags.defineBoolean('d', false, 'Decipher');
    flags.defineString('text', "Text to cipher", 'Cesar text');
    flags.defineNumber('key', 1, 'Cesar key');
    flags.parse();

    const text = flags.get("text");
    const key = flags.get("key");
    
    if(flags.get("e")) {
        const r = Cesar.cipher(text, key)
        console.log("Ciphered text: ", r);
    }

    if(flags.get("d")) {
        const r = Cesar.decipher(text, key)
        console.log("Deciphered text: ", r);
    }
    
}

module.exports = { main }