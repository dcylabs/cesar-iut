const utility = require('./utility');

function cipher(input, key) {
    return input.split('').map(char => utility.rotateOneChar(char, key)).join('');
}

function decipher(input, key) {
    return cipher(input, -1 * key)
}

module.exports = { cipher, decipher };