const Cesar = require('./cesar.js'); 

test('should cipher classic string with a standard key', () => {
    expect( Cesar.cipher("ABCDEFGHIJKLMNOPQRSTUVWXYZ", 3) ).toEqual("DEFGHIJKLMNOPQRSTUVWXYZABC");
    expect( Cesar.cipher("FIBRE OPTIQUE", 12) ).toEqual("RUNDQ ABFUCGQ");
});

test('should cipher multicase string with a standard key', () => {
    expect( Cesar.cipher("AbCdEfGhIjKlMnOpQrStUvWxYz", 3) ).toEqual("DeFgHiJkLmNoPqRsTuVwXyZaBc");
});

test('should decipher classic string with a standard key', () => {
    expect( Cesar.decipher("DEFGHIJKLMNOPQRSTUVWXYZABC", 3) ).toEqual("ABCDEFGHIJKLMNOPQRSTUVWXYZ");
    expect( Cesar.decipher("RUNDQ ABFUCGQ", 12) ).toEqual("FIBRE OPTIQUE");
});

test('should decipher multicase string with a standard key', () => {
    expect( Cesar.decipher("DeFgHiJkLmNoPqRsTuVwXyZaBc", 3) ).toEqual("AbCdEfGhIjKlMnOpQrStUvWxYz");
});

test('should decipher a ciphered string', () => {
    const originalString = "Date de la séance";
    const cipheringKey = 3;
    const cipheredString = Cesar.cipher(originalString, cipheringKey);
    expect( Cesar.decipher(cipheredString, cipheringKey) ).toEqual(originalString);
});
