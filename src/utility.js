const CHARSET = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

function rotateOneChar(char, key) {
    const refChar = char.toUpperCase();
    if(isInCharset(refChar)) return char;
    const newChar = CHARSET[(CHARSET.indexOf(refChar) + normalizeKey(key)) % CHARSET.length]
    return isUpperCase(char) ? newChar : newChar.toLowerCase();
}

function isUpperCase(char) {
    return char == char.toUpperCase();
}

function isInCharset(char) {
    return CHARSET.indexOf(char) == -1
}

function normalizeKey(key) {
    key = key % CHARSET.length 
    if(key < 0) key = key + CHARSET.length;
    return key;
}

module.exports = {
    rotateOneChar
}